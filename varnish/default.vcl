vcl 4.0;

backend default {
  .host = "backend:8080";
}

sub vcl_recv {
    if(req.url ~ ".*/wanda/api/files/v1/.*") {
        return(hash);
    }
    return(pass);
}

sub vcl_hash {
    hash_data(regsub(req.url, "^([^?]*).*", "\1"));
    if (req.http.host) {
        hash_data(req.http.host);
    }
    return(lookup);
}

sub vcl_backend_response {
  if (beresp.status < 300) {
      set beresp.ttl = 1y;
      set beresp.storage = storage.profiles;
  }
}