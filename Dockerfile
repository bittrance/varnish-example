FROM python:3.8-slim

RUN pip install bottle
COPY ./file_api.py /file_api.py

ENTRYPOINT ["/file_api.py"]