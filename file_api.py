#!/usr/bin/env python3
import logging
import sys
from bottle import Bottle, HTTPResponse, request

logger = logging.getLogger('striv')
handler = logging.StreamHandler(sys.stderr)
handler.setFormatter(logging.Formatter(
    '%(asctime)s  [%(levelname)s] %(message)s')
)
logger.addHandler(handler)
logger.setLevel('DEBUG')


app = Bottle()


def log(func):
    '''
    '''
    def wrapper(*args, **kwargs):
        logger.info(request)
        return func(*args, **kwargs)
    return wrapper


app.install(log)


@app.get('/wanda/api/files/v1/:name/:version')
def get_file(name, version):
    if name == 'boom':
        raise HTTPResponse(status=500)
    if name == 'nope':
        raise HTTPResponse(status=404)
    return name + ':' + version


@app.get('/wanda/api/files/uncached')
def get_uncached():
    return 'uncached'


if __name__ == '__main__':
    app.run(host='0.0.0.0', reloader=True, port=8080, debug=True)
