# Varnish example

This Varnish configuration acts as a pass-through for all requests except those that match a specific endpoint. Requests to that endpoint will be cached forever based on their host header and URL path, disregarding any query parameters.

```bash
docker-compose build
docker-compose up
```

You can now test this by curl:ing the API. A unique combination of name and version should result in only one request to backend:

```bash
curl 'http://localhost:8080/wanda/api/files/v1/foo/bar?some=param'
```

The special name `boom` causes the API to 500 and will not be cached.

```bash
curl 'http://localhost:8080/wanda/api/files/v1/boom/bar?some=param'
```

The `uncached` version will not be cached and will always be passed on to the backend.

```bash
curl 'http://localhost:8080/wanda/api/files/uncached'
```
